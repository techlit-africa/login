# Greeter Reference

## Original version (unmaintained) -- the one we're using
- [Unmaintained docs](https://web.archive.org/web/20190524032923/https://doclets.io/Antergos/web-greeter/stable)
- [Unmaintained repo](https://github.com/Antergos/web-greeter)

## Recent version -- not the one we're using
- [Upstream fork](https://github.com/JezerM/web-greeter)
- [Upstream docs](https://jezerm.github.io/web-greeter/howto/intro.html)

# PHP LDAP for "Add Account" feature
- [PHP LDAP](https://www.php.net/manual/en/book.ldap.php)
