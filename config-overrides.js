const {
  override,
  disableEsLint,
  addBabelPlugins,
  addPostcssPlugins,
  addWebpackPlugin,
} = require('customize-cra')

const webpack = require('webpack')

module.exports = override(
  disableEsLint(),

  ...addBabelPlugins(
    'transform-jsx-classname-components',
    'transform-react-pug',
  ),

  addPostcssPlugins([
    require('autoprefixer'),
    require('tailwindcss'),
  ]),

  addWebpackPlugin(new webpack.ProvidePlugin({
    React: 'react',
    ErrorBoundary: ['react-error-boundary', 'ErrorBoundary'],
    Icon: ['@fortawesome/react-fontawesome', 'FontAwesomeIcon'],
  })),

  addWebpackPlugin(new webpack.DefinePlugin({
    VERSION: `"${process.env.VERSION || 'development'}"`,
    NODE_ENV: `"${process.env.NODE_ENV || 'development'}"`,
    LOGIN_URL: `"${process.env.LOGIN_URL || `http://localhost:${process.env.PORT || 4000}`}"`,
  })),
)
