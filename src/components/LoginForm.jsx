import {useLoginState} from 'hooks/useLoginState'

export const LoginForm = () => {
  const {
    usernameInputRef,
    passwordInputRef,
    loginButtonRef,
    isOnline,
    isWaiting,
    message,
    error,
    userLogin,
  } = useLoginState()

  const [showPassword, setShowPassword] = React.useState(false)
  const toggleShowPassword = (event) => {
    event.preventDefault()
    setShowPassword(!showPassword)
    passwordInputRef.current?.focus()
  }

  const loginIfEnter = runIfEnter(userLogin)
  const focusPasswordIfEnterAndPresent = runIfEnter(() => {
    if (!usernameInputRef.current?.value) return
    passwordInputRef.current?.focus()
  })

  return pug`
    div(
      className=LOGIN_CONTAINER_CLASSES
    )
      .pt-6.xl_pt-8.pb-2.px-4.xl_px-6.flex.flex-col
        .pb-3.xl_4.text-2xl.xl_text-4xl.text-gray-200 Username
        input(
          ref=usernameInputRef
          autoFocus
          disabled=isWaiting
          className=(
            LOGIN_INPUT_CLASSES +
            (isWaiting ? LOGIN_WAITING_CLASSES : LOGIN_READY_CLASSES)
          )
          placeholder='username'
          type='text'
          onKeyDown=focusPasswordIfEnterAndPresent
        )

        .pt-4.xl_pt-6.pb-3.xl_pb-4.text-2xl.xl_text-4xl.text-gray-200 Password
        .relative
          input.w-full.pr-14.lg_pr-16(
            ref=passwordInputRef
            disabled=isWaiting
            className=(
              LOGIN_INPUT_CLASSES +
              (isWaiting ? LOGIN_WAITING_CLASSES : LOGIN_READY_CLASSES)
            )
            placeholder=(showPassword ? 'p@s$w0rd' : '************')
            type=(showPassword ? 'text' : 'password')
            onKeyDown=loginIfEnter
          )
          button.absolute.top-0.right-3.lg_right-4.bottom-0(
            disabled=isWaiting
            className=SHOW_PASSWORD_CLASSES
            onClick=toggleShowPassword
          )
            Icon.text-2xl.lg_text-3xl(icon=(showPassword ? 'eye' : 'eye-slash'))

        .pt-4.lg_pt-6
        button(
          ref=loginButtonRef
          disabled=isWaiting
          className=LOGIN_BUTTON_CLASSES
          onClick=userLogin
        ) Login

      if isWaiting
        .pt-1
        .px-6.py-3.bg-black.text-2xl.text-white.opacity-70.rounded-b-xl
          Icon.animate-spin-slow(icon='spinner')
          span.pl-4 Loading...

      else
        unless isOnline
          .pt-1
          .px-6.py-3.bg-black.bg-opacity-80.text-2xl.text-red-600.rounded-b-xl Network error! WiFi or Ethernet disconnected.

        else if message
          .pt-1
          .px-6.py-3.bg-black.bg-opacity-80.text-2xl.text-yellow-600.rounded-b-xl= message

        else if error
          .pt-1
          .px-6.py-3.bg-black.bg-opacity-80.text-2xl.text-red-600.rounded-b-xl= error
  `
}

const runIfEnter = (fn) => (event) =>
  event.key === 'Enter' && fn()

const LOGIN_CONTAINER_CLASSES = (
  ' w-full flex flex-col ' +
  ' bg-black bg-opacity-75 ' +
  ' border border-white border-opacity-50 ' +
  ' rounded-xl shadow overflow-y-hidden '
)

const LOGIN_INPUT_CLASSES = (
  ' py-2 px-3 lg_px-5 focus_py-3 lg_focus_py-4 xl_focus_py-5 ' +
  ' text-2xl xl_text-4xl ' +
  ' bg-black ' +
  ' focus_ring-4 active_ring-4 focus_ring-blue-500 active_ring-blue-500 ' +
  ' border border-blue-500 focus_border-none active_border-none border-opacity-50 ' +
  ' focus_outline-none active_outline-none ' +
  ' transition-all duration-300 ' +
  ' rounded-lg shadow-lg '
)

const LOGIN_READY_CLASSES = ' bg-opacity-80 '

const LOGIN_WAITING_CLASSES = ' bg-opacity-60 '

const SHOW_PASSWORD_CLASSES = (
  ' p-2 text-2xl text-white opacity-70 focus_opacity-90 ' +
  ' focus_ring-4 active_ring-4 focus_ring-blue-500 active_ring-blue-500 ' +
  ' focus_outline-none active_outline-none ' +
  ' transition-all duration-300 ' +
  ' rounded-lg '
)

const LOGIN_BUTTON_CLASSES = (
  ' my-4 py-2 lg_py-3 px-3 lg_px-4 xl_px-5 focus_py-4 lg_focus_py-5 ' +
  ' text-3xl lg_text-4xl text-blue-200 ' +
  ' bg-black bg-opacity-80 focus_bg-opacity-90 hover_bg-opacity-100 active_bg-opacity-100 ' +
  ' focus_ring-4 active_ring-4 focus_ring-blue-500 active_ring-blue-500 ' +
  ' border-2 border-blue-200 focus_border-none active_border-none border-opacity-50 ' +
  ' focus_outline-none active_outline-none ' +
  ' transition-all duration-300 ' +
  ' rounded-lg '
)
