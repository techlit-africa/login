import {ControlPanel} from 'components/ControlPanel'
import {LoginForm} from 'components/LoginForm'
import {GuestButton} from 'components/GuestButton'
import {useKeybindings} from 'hooks/useKeybindings'
import {useLoginState} from 'hooks/useLoginState'

export const Main = () => {
  const {localConfig, serverConfig, guestLogin, focusNext, focusPrev, shutdown, restart} = useLoginState()

  const bindingsRef = React.useRef([])
  bindingsRef.current = [
    {meta: true, alt: true, key: 'q', fn: shutdown},
    {meta: true, alt: true, key: 'r', fn: restart},
    {key: 'ArrowDown', fn: focusNext},
    {key: 'ArrowUp', fn: focusPrev},
    {key: 'Enter', fn: guestLogin},
  ]
  useKeybindings(bindingsRef)

  return pug`
    .w-screen.h-screen.flex.flex-col
      ControlPanel

      .w-full.h-full.p-4.text-white.flex.items-end.justify-between
        .p-0
          .text-sm (this machine)
          .text-2xl.font-medium
            if localConfig.version
              | v#{localConfig.version}
            else
              span.animate-pulse loading

        .p-0.h-full.flex.flex-col.justify-between
          div(className=CONTAINER_CLASSES)
            img.h-36.md_h-40.xl_h-52.pb-6.mx-auto(src='logo.png')
            .h-12
            GuestButton
            //.pb-6
            //LoginForm

        .p-0.text-right
          .text-sm (server)
          .text-2xl.font-medium
            if serverConfig.version
              | v#{serverConfig.version}
            else
              span.animate-pulse searching
  `
}

const CONTAINER_CLASSES = (
  ' w-96 lg_w-big xl_w-bigger mx-auto flex-1 ' +
  ' flex flex-1 flex-col items-center justify-center ' +
  ' pt-8 sm_pt-12 xl_pt-16 '
)
