import {useLightDM} from 'hooks/useLightDM'
import dayjs from 'dayjs'

export const ControlPanel = () => pug`
  React.Suspense(fallback=${pug`NotReady`})
    ErrorBoundary(fallback=${pug`NotReady`})
      Ready
`

const NotReady = () => pug`
  div(className=CONTAINER_CLASSES)
`

const Ready = () => {
  const [time, setTime] = React.useState('')
  const tick = () => setTime(dayjs().format('llll'))
  React.useEffect(() => {
    tick()
    const interval = setInterval(tick, 20000)
    return () => clearInterval(interval)
  }, [])

  const {lightdm, error} = useLightDM()
  if (error) throw error
  if (!lightdm) throw new Promise((resolve) => setTimeout(resolve, 300))

  return pug`
    div(className=CONTAINER_CLASSES)
      .text-xl= time

      .flex.flex-row.items-center.justify-between
        button(
          className=CONTROL_CLASSES
          onClick=()=>lightdm.restart()
        )
          Icon(icon='undo-alt')

        button(
          className=CONTROL_CLASSES
          onClick=()=>lightdm.shutdown()
        )
          Icon(icon='power-off')
  `
}

const CONTROL_CLASSES = (
  ' w-8 h-8 md_w-10 md_h-10 xl_w-14 xl_h-14 ' +
  ' flex items-center justify-center ' +
  ' text-lg md_text-xl xl_text-3xl '
)

const CONTAINER_CLASSES = (
  ' fixed top-0 right-0 left-0 ' +
  ' h-8 md_h-10 xl_h-14 ' +
  ' pl-4 md_pl-6 pr-2 md_pr-4 ' +
  ' flex flex-row items-center justify-between ' +
  ' text-gray-400 bg-gray-800 bg-opacity-70 ' +
  ' border-b border-white border-opacity-30 ' +
  ' shadow-lg '
)
