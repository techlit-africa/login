import {useLoginState} from 'hooks/useLoginState'

export const GuestButton = () => {
  const {isWaiting, guestButtonRef, guestLogin} = useLoginState()

  React.useEffect(() => {
    guestButtonRef.current?.focus()
  }, [guestButtonRef.current])

  return pug`
    button(
      ref=guestButtonRef
      disabled=isWaiting
      className=GUEST_BUTTON_CLASSES
      onClick=guestLogin
    )
      .text-2xl.lg_text-3xl.xl_text-4xl.pr-2.lg_pr-3.xl_pr-4 Guest login
      Icon.text-xl.lg_text-2xl.xl_text-3xl(icon='arrow-alt-circle-right')
  `
}

const GUEST_BUTTON_CLASSES = (
  //' self-end ' +
  ' flex flex-row items-center ' +
  ' py-2 lg_py-3 px-3 lg_px-4 xl_px-5 focus_py-4 lg_focus_py-5 ' +
  ' text-gray-800 hover_text-gray-900 active_text-black ' +
  ' bg-white bg-opacity-80 focus_bg-opacity-90 hover_bg-opacity-100 active_bg-opacity-100 ' +
  ' border border-black border-opacity-50 ' +
  ' focus_ring-4 active_ring-4 focus_ring-blue-500 active_ring-blue-500 ' +
  ' transition-all duration-300 ' +
  ' rounded-xl shadow-lg '
)
