import {Switch, Route, Redirect} from 'react-router-dom'
import {Main} from 'components/Main'

export const App = () => {
  return pug`
    Switch
      Route(path='/') #[Main]
      Route #[Redirect(to='/')]
  `
}
