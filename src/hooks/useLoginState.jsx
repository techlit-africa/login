import {provideContext} from 'lib/provideContext'
import {useLightDM} from './useLightDM'

export const {Provider: LoginStateProvider, useContext: useLoginState} =
  provideContext(() => {
    const {lightdm} = useLightDM()

    const usernameInputRef = React.useRef()
    const passwordInputRef = React.useRef()
    const loginButtonRef = React.useRef()
    const guestButtonRef = React.useRef()

    const serverIp = React.useRef(null)
    const [localConfig, setLocalConfig] = React.useState({})
    const [serverConfig, setServerConfig] = React.useState({})

    const [isOnline, setIsOnline] = React.useState(true)
    const [isWaiting, setIsWaiting] = React.useState(false)
    const [message, setMessage] = React.useState()
    const [error, setError] = React.useState()

    const shutdown = React.useCallback(() => {
      setIsWaiting(true)
      setError(null)
      lightdm?.shutdown()
    }, [lightdm])

    const restart = React.useCallback(() => {
      setIsWaiting(true)
      setError(null)
      lightdm?.restart()
    }, [lightdm])

    const focusNext = React.useCallback(() => {
      const u = usernameInputRef.current
      const p = passwordInputRef.current
      const l = loginButtonRef.current
      const g = guestButtonRef.current
      if (isWaiting || !u || !p || !l || !g) return

      if (g === document.activeElement) return u.focus()
      if (u === document.activeElement) return p.focus()
      if (p === document.activeElement) return l.focus()
      if (l === document.activeElement) return g.focus()
      return u.focus()
    }, [isWaiting])

    const focusPrev = React.useCallback(() => {
      const u = usernameInputRef.current
      const p = passwordInputRef.current
      const l = loginButtonRef.current
      const g = guestButtonRef.current
      if (isWaiting || !u || !p || !l || !g) return

      if (g === document.activeElement) return l.focus()
      if (u === document.activeElement) return g.focus()
      if (p === document.activeElement) return u.focus()
      if (l === document.activeElement) return p.focus()
      return p.focus()
    }, [isWaiting])

    const userLogin = React.useCallback(() => {
      const u = usernameInputRef.current
      const p = passwordInputRef.current
      if (isWaiting || !u || !p || !lightdm) return

      setIsWaiting(true)
      setError(null)
      lightdm.authenticate()
    }, [isWaiting, lightdm])

    const guestLogin = React.useCallback(() => {
      //if (isWaiting) return

      setIsWaiting(true)
      setError(null)
      //lightdm.authenticate_as_guest()
      console.log('logging in now...')
      lightdm.authenticate_as_guest()
      setTimeout(() => lightdm.start_session('cinnamon'), 100)
      console.log('ok')
    }, [isWaiting, lightdm])

    React.useEffect(() => {
      const pingServer = async () => {
        const controller = new window.AbortController()
        const timeoutId = setTimeout(() => controller.abort(), 1000)

        try {
          const url = `http://${serverIp.current || 'localhost'}/techlit/config.json`
          const rep = await window.fetch(url, {signal: controller.signal})
          const config = await rep.json()

          if (serverIp.current) {
            setIsOnline(true)
            setServerConfig(config)
          } else {
            setLocalConfig(config)
            serverIp.current = config.serverIp
          }
        } catch (e) {
          console.error(e)
          setIsOnline(false)
        } finally {
          clearTimeout(timeoutId)
          setTimeout(pingServer, 5000)
        }
      }
      pingServer()
    }, [])

    //React.useEffect(() => {
    //  window.show_prompt = (prompt, type) => {
    //    if (prompt === 'login:') {
    //      return lightdm.respond(usernameInputRef.current?.value)
    //    }
    //    if (prompt === 'Password: ') {
    //      return lightdm.provide_secret(passwordInputRef.current?.value)
    //    }
    //    console.error(`Unknown ${type} prompt: ${prompt}`)
    //    setError('Something went wrong. Try again later, or use a guest session.')
    //  }

    //  window.show_message = (message, type) =>
    //    (type === 'error' ? setError : setMessage)(message)

    //  window.authentication_complete = () => {
    //    const u = usernameInputRef.current
    //    const p = passwordInputRef.current
    //    if (!u || !p || !lightdm) return

    //    if (lightdm?.is_authenticated) {
    //      setError(null)
    //      return lightdm?.start_session_sync('cinnamon')
    //    }

    //    u.value = ''
    //    p.value = ''

    //    setIsWaiting(false)
    //    setError('Wrong username or password')
    //    u.focus()
    //  }
    //}, [lightdm])

    return {
      usernameInputRef,
      passwordInputRef,
      loginButtonRef,
      guestButtonRef,

      localConfig,
      serverConfig,

      isOnline,
      isWaiting,
      message,
      error,

      shutdown,
      restart,

      focusNext,
      focusPrev,

      userLogin,
      guestLogin,
    }
  })
