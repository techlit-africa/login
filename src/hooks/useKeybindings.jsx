export const useKeybindings = (bindingsRef) => {
  React.useEffect(() => {
    const onKeyDown = (event) => {
      for (const {alt, ctrl, meta, shift, key, fn} of bindingsRef.current) {
        if (
          !!alt === event.altKey &&
            !!ctrl === event.ctrlKey &&
            !!meta === event.metaKey &&
            !!shift === event.shiftKey &&
            key === event.key
        ) {
          event.preventDefault()
          fn?.()
        }
      }
    }

    document.addEventListener('keydown', onKeyDown)
    return () => document.removeEventListener('keydown', onKeyDown)
  }, [])
}
