import {provideContext} from 'lib/provideContext'
import {mockLightDM} from 'lib/mockLightDM'

const INTERVAL = 300
const TIMEOUT = 1200
const SHOULD_MOCK = true

export const {Provider: LightDMProvider, useContext: useLightDM} =
  provideContext(() => {
    const [state, setState] = React.useState({})

    React.useEffect(() => {
      let timeWaited = 0
      if (NODE_ENV === 'development' && SHOULD_MOCK) mockLightDM()

      const waitForLightDM = () => {
        if (timeWaited > TIMEOUT) {
          const error = new Error(`lightdm wasn't available after ${TIMEOUT}ms`)
          setState({error})
          return
        }

        if (!window.lightdm) {
          timeWaited += INTERVAL
          setTimeout(() => waitForLightDM(), INTERVAL)
          return
        }

        setState({
          config: window.config,
          lightdm: window.lightdm,
          greeterUtil: window.greeterutil,
        })
      }

      waitForLightDM()
    }, [])

    return state
  })
