import {render} from 'react-dom'
import {BrowserRouter} from 'react-router-dom'
import {SWRProvider} from 'lib/swr'
import {LoginStateProvider} from 'hooks/useLoginState'
import {LightDMProvider} from 'hooks/useLightDM'
import {App} from 'components/App'

import 'styles/index.css'
import 'init/fontawesome'
import 'init/dayjs'

const Main = () => {
  return pug`
    React.StrictMode
      BrowserRouter
        SWRProvider
          LightDMProvider
            LoginStateProvider
              App
  `
}

render(pug`Main`, document.getElementById('root'))
