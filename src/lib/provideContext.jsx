export const provideContext = (provide) => {
  const Context = React.createContext({})

  const Provider = (props) => {
    const provided = provide(props)
    return (
      <Context.Provider value={provided}>{props.children}</Context.Provider>
    )
  }

  const Consumer = Context.Consumer

  const useContext = () => React.useContext(Context)

  return {Context, Provider, Consumer, useContext}
}
