import dayjs from 'dayjs'

export const mockLightDM = () => {
  window.greeter_config = {
    ...mockConfig,
    get_str: getFromMockConfig,
    get_bool: getFromMockConfig,
    get_num: getFromMockConfig,
  }

  window.greeterutil = {
    dirlist: (_dir) => [],
    get_current_localized_time: () => dayjs().format('llll'),
    esc_html: (text) => text,
    txt2html: (text) => text,
  }

  window.lightdm = {
    authenticate: (username = null) => {
      console.log('starting authentication...')
      window.lightdm.authentication_user = username
      window.lightdm.is_authenticated = false
      window.start_authentication?.()
      window.show_prompt?.('login:', 'text')
    },

    authenticate_as_guest: () => {
      console.log('authenticating as guest...')
      window.lightdm.is_authenticated = true
      setTimeout(() => {
        window.authentication_complete?.()
      }, 1000)
    },

    cancel_authentication: () => {
      console.log('cancelling authentication...')
      window.lightdm.authentication_user = null
      window.lightdm.is_authenticated = false
      window.cancel_authentication?.()
    },
    cancel_autologin: () => {},
    cancel_timed_login: () => {},

    get_hint: () => {},
    login: () => {},
    provide_secret: (secret) => {
      const okay = secret.startsWith('e')
      window.lightdm.is_authenticated = okay
      setTimeout(() => {
        if (!okay) {
          window.show_message?.('Bad password', 'error')
        }
        window.authentication_complete?.()
      }, 1000)
    },
    respond: (response) => {
      if (!window.lightdm.authentication_user) {
        window.lightdm.authentication_user = response
        window.show_prompt?.('Password: ', 'password')
      } else {
        const okay = response.startsWith('e')
        window.lightdm.is_authenticated = okay
        setTimeout(() => {
          if (!okay) {
            window.show_message?.('Bad password', 'error')
          }
          window.authentication_complete?.()
        }, 1000)
      }
    },

    start_session: () => {
      window.alert('logging in...')
      return true
    },
    start_session_sync: () => {
      window.alert('logging in...')
      return true
    },

    set_language: () => true,

    restart: () => window.alert('restarting...'),
    hibernate: () => window.alert('hibernating...'),
    shutdown: () => window.alert('shutting down...'),
    suspend: () => window.alert('suspending...'),

    authentication_user: null,

    autologin_guest: false,
    autologin_timeout: 0,
    autologin_user: null,

    can_hibernate: false,
    can_restart: true,
    can_shutdown: true,
    can_suspend: false,

    default_language: 'British English',
    default_layout: 'us',
    default_session: 'cinnamon',

    has_guest_account: true,
    hide_users: true,

    hostname: 'techlit-12345.techlitafrica.org',

    in_authentication: false,
    is_authenticated: false,

    language: 'British English',
    languages: [
      ['English', 'en', null],
      ['British English', 'en_GB', 'United Kingdom'],
      ['French', 'fr_FR', 'France'],
    ].map(({name, code, territory}) =>
      ({name, code, territory})),

    layout: 'us',
    layouts: [
      ['us', 'en', 'English (US)'],
      ['gb', 'en', 'English (UK)'],
      ['fr', 'fr', 'French (FR)'],
    ].map(({name, short_description, description}) =>
      ({name, short_description, description})),

    lock_hint: false,
    num_users: 1,

    select_guest: false,
    select_guest_hint: false,

    select_user: null,
    select_user_hint: null,

    session_starting: false,
    sessions: [
      ['Cinnamon', 'cinnamon', 'This session logs you into Cinnamon'],
      ['XFCE', 'xfce', 'This session logs you into XFCE'],
    ],

    start_authentication: () => {},

    timed_login_delay: 0,
    timed_login_user: null,

    users: [
      ['TechLit', '/home/techlit', '/logo.png', 'en_GB', null, false, 'techlit', 'TechLit', 'cinnamon', 'techlit'],
    ].map(({display_name, home_directory, image, language, layout, logged_in, name, real_name, session, username}) =>
      ({display_name, home_directory, image, language, layout, logged_in, name, real_name, session, username})),
  }
}

const getFromMockConfig = (section, key, type = '') => {
  const val = (mockConfig[section] || {})[key]
  // eslint-disable-next-line valid-typeof
  if (typeof val !== type) {
    throw new TypeError(`${key} in ${section} isn't ${type}`)
  }
  return val
}

const mockConfig = {
  greeter: {
    debug_mode: true,
    detect_theme_errors: true,
    screensaver_timeout: 300,
    secure_mode: false,
    time_format: 'LT',
    time_language: 'auto',
    webkit_theme: 'techlit',
  },
  branding: {
    logo: '/logo.png',
    user_image: '/logo.png',
    background_images: '/',
  },
}
