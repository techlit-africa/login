export const fetch = async (path, ops = {}) => {
  const rep = await window.fetch(`${PDD_URL}${path}`, {
    ...ops,
    headers: {
      'Content-Type': 'application/json',
      ...(ops.headers || {}),
    },
  })
  return await rep.json()
}
