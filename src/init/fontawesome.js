import {library, config} from '@fortawesome/fontawesome-svg-core'

import {
  faSpinner,
  faEye,
  faEyeSlash,
  faUndoAlt,
  faPowerOff,
  faArrowAltCircleRight,
} from '@fortawesome/free-solid-svg-icons'

library.add(
  faSpinner,
  faEye,
  faEyeSlash,
  faUndoAlt,
  faPowerOff,
  faArrowAltCircleRight,
)

config.autoAddCss = false
