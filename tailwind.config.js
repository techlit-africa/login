const colors = require('tailwindcss/colors')

module.exports = {
  // mode: "jit",
  separator: '_',
  purge: ['./**/*.{js,ts,jsx,tsx}'],
  darkMode: 'class',
  variants: {
    extend: {
      display: ['dark'],
      textColor: ['hover', 'active'],
      borderOpacity: ['hover', 'active'],
      backgroundOpacity: ['hover', 'active'],
      backgroundColor: ['active'],
      padding: ['focus'],
      fontSize: ['focus'],
    },
  },
  theme: {
    transparent: 'transparent',
    current: 'currentColor',
    ...colors,
    extend: {
      outline: {
        primary: [`2px solid ${colors.amber[500]}`, '0px'],
      },
      animation: {
        'spin-slow': 'spin 2s linear infinite',
      },
      spacing: {
        big: '32rem',
        bigger: '38rem',
      },
    },
  },
  plugins: [],
}
